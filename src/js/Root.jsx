import React from 'react';
import { HashRouter as Router } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';

// Styles
// Import fonts from react-redux-boilerplate
import '../assets/fonts/fonts.scss';
// Import Main styles for this application
import '../style/style.scss'
// Temp fix for reactstrap
import '../style/core/_dropdown-menu-right.scss'

function Root(props) {
  return (
    <Provider store={props.store}>
      <Router>
        {props.routes}
      </Router>
    </Provider>
  );
}

Root.propTypes = {
  routes: PropTypes.element.isRequired,
  store: PropTypes.object.isRequired,
};

export default Root;
