import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { GoogleLogin } from 'react-google-login';
import _get from 'lodash/get';

import { googleLogin, loginFailure } from '../../../redux/modules/session';

import './GoogleLoginButton.scss';

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  googleLogin,
  loginFailure,
};

/**
 * 
 * Google Login Button Component
 * 
 * @author mgea
 */

@connect(mapStateToProps, mapDispatchToProps)
class GoogleLoginButton extends PureComponent {
  constructor(props) {
    super(props);
    this.googleFailure = this.googleFailure.bind(this);
    this.googleSuccess = this.googleSuccess.bind(this);
  }

  googleFailure(err) {
    const errorMessage = _get(err, 'error', err.reason || '');
    this.props.loginFailure(errorMessage);
  }

  googleSuccess(googleUser) {
    const authToken = googleUser.getAuthResponse().access_token;
    this.props.googleLogin({ authToken });
  }

  render() {
    const { clientID, hostedDomain } = __CONFIG__;

    return (
      <div className="loginButton">
        <GoogleLogin
          clientId={clientID}
          className="google-login btn btn-primary px-4"
          scope="profile"
          hostedDomain={hostedDomain}
          onSuccess={this.googleSuccess}
          onFailure={this.googleFailure}
          buttonText="Login with Google"
        />
      </div>
    );
  }
}

export default GoogleLoginButton;
