import React from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';

/**
 * 
 * Training List Component
 * 
 * @author mgea
 * @param sessionItem
 */
function SessionsListItem({ sessionItem }) {
  const parsedSpeakers = sessionItem.speakers.map((sl) => { return ` ${sl.first_name} ${sl.last_name}` });
  return (
    <li>
      <div>{sessionItem.location.name}</div>
      <div><Moment format="MM/DD/YYYY h:mm a">{sessionItem.date}</Moment></div>
      <div>Speakers: {parsedSpeakers}</div>
    </li>
  );
}

SessionsListItem.propTypes = {
  sessionItem: PropTypes.object.isRequired,
}

export default SessionsListItem;
