import React, { Component } from 'react';
import { SidebarSearchForm } from '../../../CoreUI';

/**
 * Training Search Form Component
 * 
 * @author mgea
 */
class TrainingSidebarSearchForm extends Component {
  static handleSearch(searchTerm) {
    console.log('Search term: ', searchTerm);
  }

  render() {
    return (
      <SidebarSearchForm onSearch={TrainingSidebarSearchForm.handleSearch} />
    )
  }
}

export default TrainingSidebarSearchForm;
