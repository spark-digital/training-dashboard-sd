import React from 'react';
import PropTypes from 'prop-types';
import _get from 'lodash/get';
import { TrainingsListItem } from '../TrainingsListItem';

/**
 * 
 * Training List Component
 * 
 * @author mgea
 * @param training
 */
function TrainingsList({ training }) {
  const list = _get(training, 'list', []);
  const trainingsListItems = list.map(t => <TrainingsListItem trainingItem={t} key={t.topic} />);
  return list.length
    ? (
      <div id="trainingsList">
        <h1>Upcoming trainings</h1>
        <ul>{trainingsListItems}</ul>
      </div>
    )
    : <h1>There are not available trainings.</h1>;
}

TrainingsList.propTypes = {
  training: PropTypes.object.isRequired,
}
export default TrainingsList;
