import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Collapse, CardBody, Card } from 'reactstrap';
import { SessionsList } from '../SessionsList';

const humanizeDuration = require('humanize-duration');
const TimeFormat = require('hh-mm-ss');

/**
 * Training List Item Component
 * 
 * @author mgea
 */
class TrainingsListItem extends PureComponent {

  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = { collapse: false };
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  render() {
    const { trainingItem } = this.props;
    const duration = trainingItem.duration ? humanizeDuration(TimeFormat.toMs(trainingItem.duration, 'hh:mm:ss')) : 'TBD';

    return (
      <li>
        <p onClick={this.toggle}>
          <strong>{trainingItem.topic}</strong>
        </p>
        <Collapse isOpen={this.state.collapse}>
          <Card>
            <CardBody>
              <ul>
                <li>Training Type: {trainingItem.type}</li>
                <li>What you are going to learn: {trainingItem.description}</li>
                <li>Intended Audience: {trainingItem.tech_stack} {trainingItem.seniority}</li>
                <li>Training Area: {trainingItem.area}</li>
                <li>Duration: {duration}</li>
                <li><SessionsList sessionsList={trainingItem.sessions_list} /></li>
              </ul>
            </CardBody>
          </Card>
        </Collapse>
      </li>
    );
  }
}

TrainingsListItem.propTypes = {
  trainingItem: PropTypes.object.isRequired,
}
export default TrainingsListItem;
