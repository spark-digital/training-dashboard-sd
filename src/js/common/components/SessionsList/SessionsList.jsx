import React from 'react';
import PropTypes from 'prop-types';
import { SessionsListItem } from '../SessionsListItem';

/**
 * 
 * Sessions List Component
 * 
 * @author mgea
 * @param sessionsList
 */
function SessionsList({ sessionsList }) {
  if (sessionsList && sessionsList.length) {
    const sessionsListItems = sessionsList.map(s => <SessionsListItem sessionItem={s} key={s.id} />);
    return (
      <div id="sessionsList">
        <h6>Sessions:</h6>
        <ul>{sessionsListItems}</ul>
      </div>
    );
  }
  return <div>There are no available sessions.</div>;
}

SessionsList.propTypes = {
  sessionsList: PropTypes.array,
}

SessionsList.defaultProps = {
  sessionsList: [],
};

export default SessionsList;
