import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import { session } from './modules';
import training from './modules/training/training';

export default combineReducers({
  session,
  training,
  routing,
});
