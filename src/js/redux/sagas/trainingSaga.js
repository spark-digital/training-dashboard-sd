import { call, put, fork, takeLatest } from 'redux-saga/effects';
import { constants as trainingConstants, actions as trainingActions } from '../modules/training/training';

import * as api from '../../utility/api';

/**
 * Get training data
 * 
 * @author mgea
 */
export function* fetchTrainingsData() {
  const list = yield call(api.getTrainings);
  yield put(trainingActions.storeTrainings(list));
}

/**
 * Watch latest training data
 * 
 * @author mgea
 */
function* watchGetTrainings() {
  yield takeLatest(trainingConstants.GET_TRAININGS, fetchTrainingsData);
}

export const trainingSaga = [
  fork(watchGetTrainings),
];
