import { all } from 'redux-saga/effects';

import { trainingSaga } from './trainingSaga';
import { sessionSaga } from './sessionSaga';

export default function* sagas() {
  yield all([
    ...sessionSaga,
    ...trainingSaga,
  ]);
}
