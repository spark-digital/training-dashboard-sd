import { call, put, fork, takeLatest } from 'redux-saga/effects';
import { constants as sessionConstants } from '../modules/session';

import * as api from '../../utility/api';

/**
 * Handle Google Session authentication 
 * 
 * @author mgea
 */
export function* fetchGoogleSession(data) {
  try {
    const authResult = yield call(api.googleLogin, data.payload.authToken);
    yield put({ type: sessionConstants.LOGIN_SUCCESS, payload: authResult });
  } catch (e) {
    yield put({ type: sessionConstants.LOGIN_FAILURE, payload: 'Login service error!' })
  }
}

/**
 * Handle Google Session Logout
 * 
 * @author mgea
 */
export function* sessionLogout() {
  try {
    yield call(api.logout);
  } catch (e) {
    console.error('Unable to logout!!!', e);
  }
}

/**
 * Watch latest session login
 * 
 * @author mgea
 */
function* watchGoogleLogin() {
  yield takeLatest(sessionConstants.GOOGLE_LOGIN, fetchGoogleSession);
}

/**
 * Watch latest session logout
 * 
 * @author mgea
 */
function* watchLogout() {
  yield takeLatest(sessionConstants.LOGOUT, sessionLogout);
}

export const sessionSaga = [
  fork(watchGoogleLogin),
  fork(watchLogout),
];
