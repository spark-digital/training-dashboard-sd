import { createSelector } from 'reselect';

const trainingsDataSelector = state => state.training;

/**
 * Training data list selector
 * 
 * @author mgea
 */
const listSelector = createSelector(
  trainingsDataSelector,
  (payload) => { 
    return payload.get('list').toJS()
  }
);

export const trainingsSelector = state => ({
  list: listSelector(state),
});

