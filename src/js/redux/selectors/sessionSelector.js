import { createSelector } from 'reselect';
import _get from 'lodash/get';

const getSession = state => _get(state, 'session', null);

/**
 * Session Selector: Is authenticated ?
 * 
 * @author mgea
 */
const isAuthenticatedSelector = createSelector(
  getSession,
  session => session && session.getIn(['user', 'token'])
);

/**
 * Session Selector: Is Loading ?
 * 
 * @author mgea
 */
const isLoadingSelector = createSelector(
  getSession,
  session => session.get('isLoading')
)

/**
 * Session Selector: Login Error
 * 
 * @author mgea
 */
const loginErrorSelector = createSelector(
  getSession,
  session => session.get('loginError')
)

/**
 * Get user data Selector
 * 
 * @author mgea
 */
const getUserDataSelector = createSelector(
  getSession,
  (session) => {
    const userData = session.get('user').toObject();
    const user = {
      ...userData,
      name: `${userData.first_name} ${userData.last_name}`,
    }
    return user;
  }
);

export const isAuthenticated = state => isAuthenticatedSelector(state);
export const isLoading = state => isLoadingSelector(state);
export const loginError = state => loginErrorSelector(state);
export const getUserData = state => getUserDataSelector(state);
