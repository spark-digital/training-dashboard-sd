import { createAction, handleActions } from 'redux-actions';
import { Map, List } from 'immutable';

const GET_TRAININGS = 'app/training/GET_TRAININGS';
const STORE_TRAININGS = 'app/training/STORE_TRAININGS';

export const constants = {
  GET_TRAININGS,
  STORE_TRAININGS,
};

// ------------------------------------
// Actions
// ------------------------------------
export const getTrainings = createAction(GET_TRAININGS, () => ({}));
export const storeTrainings = createAction(STORE_TRAININGS, (list : Array) => ({ list }));

export const actions = {
  getTrainings,
  storeTrainings,
};

export const reducers = {
  [STORE_TRAININGS]: (state, { payload }) =>
    state.merge({
      ...payload,
    }),
}

export const initialState = () =>
  Map({
    list: List(),
  })

export default handleActions(reducers, initialState());
