import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';

const GOOGLE_LOGIN = 'app/session/GOOGLE_LOGIN';
const LOGOUT = 'app/session/LOGOUT';
const LOGIN_SUCCESS = 'app/session/LOGIN_SUCCESS';
const LOGIN_FAILURE = 'app/session/LOGIN_FAILURE';

export type GoogleLoginRequest = {
  authToken: string
};

export const constants = {
  GOOGLE_LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT,
};

// ------------------------------------
// Actions
// ------------------------------------
export const googleLogin = createAction(GOOGLE_LOGIN, (req: GoogleLoginRequest) => req);
export const logout = createAction(LOGOUT);
export const loginSuccess = createAction(LOGIN_SUCCESS);
export const loginFailure = createAction(LOGIN_FAILURE, (error: string) => error);

export const actions = {
  googleLogin,
  logout,
  loginSuccess,
  loginFailure,
};

// ------------------------------------
// Reducers
// ------------------------------------
export const reducers = {
  [GOOGLE_LOGIN]: state =>
    state.merge({
      isLoading: true,
    }),
  [LOGIN_SUCCESS]: (state, { payload }) =>
    state.merge({
      user: { ...payload },
      isLoading: false,
      loginError: null,
    }),
  [LOGIN_FAILURE]: (state, { payload }) =>
    state.merge({
      user: null,
      isLoading: false,
      loginError: payload,
    }),
  [LOGOUT]: state =>
    state.merge({
      user: null,
    }),
}

export const initialState = () =>
  Map({
    user: {},
    isLoading: false,
  })

export default handleActions(reducers, initialState());
