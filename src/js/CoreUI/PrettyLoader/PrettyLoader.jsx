/* eslint-disable import/no-extraneous-dependencies */
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import Spinner from 'react-spinkit';

import './PrettyLoader.scss';

class PrettyLoader extends PureComponent {
  render() {
    const hideOverlay = this.props.show ? '' : 'hidden';
    const loaderClasses = `PrettyLoaderOverlay ${hideOverlay}`;
    return (
      <Fragment>
        {this.props.children}
        <div className={loaderClasses}>
          <Spinner className="PrettyLoader" fadeIn="quarter" name="pacman" color="yellow" />
        </div>
      </Fragment>
    );
  }
}

PrettyLoader.defaultProps = {
  show: false,
};

PrettyLoader.propTypes = {
  show: PropTypes.bool,
};

export default PrettyLoader;
