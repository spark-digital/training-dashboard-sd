/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';

function Footer() {
  return (
    <footer className="app-footer">
      <span>Training App &copy; 2018 Spark Digital.</span>
    </footer>
  )
}

export default Footer;
