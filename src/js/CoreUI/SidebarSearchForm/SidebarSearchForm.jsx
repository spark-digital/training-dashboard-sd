/* eslint-disable import/no-extraneous-dependencies */
import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Form, InputGroup, Input, InputGroupAddon, Button } from 'reactstrap';

class SidebarForm extends Component {
  constructor(props) {
    super(props);
    this.handleSearch = this.handleSearch.bind(this);
    this.updateSearchTerm = this.updateSearchTerm.bind(this);
  }

  handleSearch(e) {
    this.props.onSearch(this.state.searchTerm);
    e.preventDefault();
  }

  updateSearchTerm(e) {
    this.setState({
      searchTerm: e.target.value,
    })
  }

  render() {
    return (
      <Form onSubmit={this.handleSearch} className="sidebar-form">
        <InputGroup>
          <Input onChange={this.updateSearchTerm} placeholder="Search..." />
          <InputGroupAddon addonType="append">
            <Button color="secondary">
              <i className="fa fa-search" />
            </Button>
          </InputGroupAddon>
        </InputGroup>
      </Form>
    )
  }
}

SidebarForm.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

export default SidebarForm;
