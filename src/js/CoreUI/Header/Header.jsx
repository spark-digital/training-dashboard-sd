/* eslint-disable import/no-extraneous-dependencies */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Nav,
  NavbarBrand,
  NavbarToggler,
} from 'reactstrap';
import HeaderDropdown from './HeaderDropdown';

class Header extends Component {
  static sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  static sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-minimized');
  }

  static mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  render() {
    const { userMenu, userData, onMenuItemClick } = this.props;
    return (
      <header className="app-header navbar">
        <NavbarToggler className="d-lg-none" onClick={Header.mobileSidebarToggle}>
          <span className="navbar-toggler-icon" />
        </NavbarToggler>
        <NavbarToggler className="d-md-down-none mr-auto" onClick={Header.sidebarToggle}>
          <span className="navbar-toggler-icon" />
        </NavbarToggler>

        <NavbarBrand href={this.props.brandUrl} />

        <Nav className="ml-auto" navbar>
          <HeaderDropdown
            onItemClick={onMenuItemClick}
            userMenu={userMenu}
            userData={userData}
          />
        </Nav>
      </header>
    );
  }
}

Header.defaultProps = {
  brandUrl: '#',
  userData: {},
};

Header.propTypes = {
  brandUrl: PropTypes.string,
  userMenu: PropTypes.array.isRequired,
  userData: PropTypes.object,
  onMenuItemClick: PropTypes.func.isRequired,
};

export default Header;
