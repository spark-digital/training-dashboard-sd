/* eslint-disable import/no-extraneous-dependencies */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
} from 'reactstrap';

class HeaderDropdown extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false,
    };
  }

  optionsList(options) {
    return options.map((item) => {
      return (
        <DropdownItem
          key={item.name}
          onClick={e => this.props.onItemClick(item, e)}
        >
          <i className={item.icon} /> {item.name}
        </DropdownItem>
      )
    });
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  render() {
    const { userMenu, userData } = this.props;
    return (
      <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle nav>
          {userData.name} <i className="fa fa-user img-avatar" style={{ fontSize: '2em', height: 'auto', verticalAlign: 'middle' }} />
        </DropdownToggle>
        <DropdownMenu right>
          {this.optionsList(userMenu)}
        </DropdownMenu>
      </Dropdown>
    );
  }
}

HeaderDropdown.propTypes = {
  userMenu: PropTypes.array.isRequired,
  userData: PropTypes.object.isRequired,
  onItemClick: PropTypes.func.isRequired,
};

export default HeaderDropdown;
