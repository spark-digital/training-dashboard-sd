/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import PropTypes from 'prop-types';
import { NavbarBrand } from 'reactstrap';

function UnauthenticatedHeader(props) {
  return (
    <header className="app-header navbar">
      <NavbarBrand href={props.brandUrl} />
    </header>
  );
}

UnauthenticatedHeader.defaultProps = {
  brandUrl: '#',
};

UnauthenticatedHeader.propTypes = {
  brandUrl: PropTypes.string,
};

export default UnauthenticatedHeader;
