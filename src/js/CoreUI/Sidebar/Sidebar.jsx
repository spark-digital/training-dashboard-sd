/* eslint-disable import/no-extraneous-dependencies, class-methods-use-this */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { Badge, Nav, NavItem, NavLink as RsNavLink } from 'reactstrap';
import classNames from 'classnames';
import SidebarMinimizer from './../SidebarMinimizer';

class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.activeRoute = this.activeRoute.bind(this);
    this.hideMobile = this.hideMobile.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    e.target.parentElement.classList.toggle('open');
  }

  activeRoute(routeName, props) {
    return props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown';
  }

  hideMobile() {
    if (document.body.classList.contains('sidebar-mobile-show')) {
      document.body.classList.toggle('sidebar-mobile-show')
    }
  }

  render() {
    // badge addon to NavItem
    const badge = (badgeData) => {
      if (badgeData) {
        const classes = classNames(badgeData.class);
        return (<Badge className={classes} color={badgeData.variant}>{ badgeData.text }</Badge>)
      }
      return null;
    };

    // simple wrapper for nav-title item
    const wrapper = (item) => {
      return (item.wrapper && item.wrapper.element
        ? (React.createElement(item.wrapper.element, item.wrapper.attributes, item.name))
        : item.name
      )
    };

    // nav list section title
    const title = (titleData, key) => {
      const classes = classNames('nav-title', titleData.class);
      return (<li key={key} className={classes}>{wrapper(titleData)} </li>);
    };

    // nav list divider
    const divider = (dividerData, key) => {
      const classes = classNames('divider', dividerData.class);
      return (<li key={key} className={classes} />);
    };

    const isExternal = (url) => {
      const link = url ? url.substring(0, 4) : '';
      return link === 'http';
    };

    // nav link
    const navLink = (item, key, classes) => {
      const url = item.url ? item.url : '';
      return (
        <NavItem key={key} className={classes.item}>
          { isExternal(url) ?
            <RsNavLink href={url} className={classes.link} active>
              <i className={classes.icon} />{item.name}{badge(item.badge)}
            </RsNavLink>
            :
            <NavLink to={url} className={classes.link} activeClassName="active" onClick={this.hideMobile}>
              <i className={classes.icon} />{item.name}{badge(item.badge)}
            </NavLink>
          }
        </NavItem>
      )
    };

    // nav label with nav link
    const navLabel = (item, key) => {
      const classes = {
        item: classNames('hidden-cn', item.class),
        link: classNames('nav-label', item.class ? item.class : ''),
        icon: classNames(
          !item.icon ? 'fa fa-circle' : item.icon,
          item.label.variant ? `text-${item.label.variant}` : '',
          item.label.class ? item.label.class : ''
        ),
      };
      return (
        navLink(item, key, classes)
      );
    };

    // nav item with nav link
    const navItem = (item, key) => {
      const classes = {
        item: classNames(item.class),
        link: classNames('nav-link', item.variant ? `nav-link-${item.variant}` : ''),
        icon: classNames(item.icon),
      };
      return (
        navLink(item, key, classes)
      )
    };

    // nav type
    /* eslint-disable no-nested-ternary, no-confusing-arrow, no-use-before-define */
    const navType = (item, idx) =>
      item.title ? title(item, idx) :
        item.divider ? divider(item, idx) :
          item.label ? navLabel(item, idx) :
            item.children ? navDropdown(item, idx) :
              navItem(item, idx);

    // nav list
    const navList = (items) => {
      return items.map((item, index) => navType(item, index));
    };

    // nav dropdown
    const navDropdown = (item, key) => {
      return (
        <li key={key} className={this.activeRoute(item.url, this.props)}>
          <button className="nav-link nav-dropdown-toggle" href="#" onClick={this.handleClick}><i className={item.icon} />{item.name}</button>
          <ul className="nav-dropdown-items">
            {navList(item.children)}
          </ul>
        </li>)
    };

    const HeaderComponent = this.props.headerComponent;
    const FormComponent = this.props.formComponent;
    const FooterComponent = this.props.footerComponent;
    // sidebar-nav root
    return (
      <div className="sidebar">
        <HeaderComponent />
        <FormComponent />
        <Nav className="sidebar-nav">
          {navList(this.props.nav)}
        </Nav>
        <FooterComponent />
        <SidebarMinimizer />
      </div>
    )
  }
}

Sidebar.defaultProps = {
  headerComponent: () => null,
  formComponent: () => null,
  footerComponent: () => null,
};

Sidebar.propTypes = {
  nav: PropTypes.array.isRequired,
  headerComponent: PropTypes.func,
  formComponent: PropTypes.func,
  footerComponent: PropTypes.func,
};

export default Sidebar;
