import React, { PureComponent } from 'react';

/**
 * Home page for authenticated users
 * 
 * @author mgea
 */
class HomeView extends PureComponent {
  render() {
    return (
      <h1>Hello {this.props.name}! You are logged in to the app</h1>
    )
  }
}

export default HomeView;
