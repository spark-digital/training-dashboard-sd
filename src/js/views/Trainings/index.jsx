/* eslint-disable import/no-extraneous-dependencies */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { actions as trainingActions } from '../../redux/modules/training/training';
import { trainingsSelector } from '../../redux/selectors/trainingsSelector';
import { TrainingsList } from '../../common/components/TrainingsList';

const mapStateToProps = state => ({
  training: trainingsSelector(state),
});

const mapDispatchToProps = {
  ...trainingActions,
};

@connect(mapStateToProps, mapDispatchToProps)
class TrainingsView extends Component {
  componentDidMount() {
    this.props.getTrainings();
  }

  render() {
    return (
      <TrainingsList {...this.props} />
    )
  }
}

export default TrainingsView;
