import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { Alert, Row, Col, Card, CardBody, Button } from 'reactstrap';

import { SlimContainer } from '../../../containers';
import { actions as sessionActions } from '../../../redux/modules/session';
import { isAuthenticated, isLoading, loginError } from '../../../redux/selectors/sessionSelector';
import { GoogleLoginButton } from '../../../common/components/GoogleLoginButton';

import { PrettyLoader } from '../../../CoreUI';

import './Login.scss';

const mapStateToProps = state => ({
  isAuthenticated: isAuthenticated(state),
  isLoading: isLoading(state),
  loginError: loginError(state),
});

const mapDispatchToProps = {
  ...sessionActions,
};

/**
 * Login page to manage authentication or redirect the authenticated user
 * 
 * @author mgea
 */
@connect(mapStateToProps, mapDispatchToProps)
class LoginView extends Component {
  get content() {
    return (
      <PrettyLoader show={this.props.isLoading}>
        <div className="loginContainer">
          <Card className="p-4">
            <CardBody>
              <h2>Login to Training Dashboard</h2>
              <p className="text-muted">Use your Google (Spark Digital) account to login</p>
              {this.props.loginError ? (
                <Alert color="danger">Error: {this.props.loginError}</Alert>
              ) : null}
              <Row>
                <Col xs="6">
                  <GoogleLoginButton />
                </Col>
                <Col xs="6" className="text-right">
                  <Button color="link" className="px-0">Login issues?</Button>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </div>
      </PrettyLoader>
    );
  }

  render() {
    return this.props.isAuthenticated ? (
      <Redirect to="/" />
    ) : (
      <SlimContainer {...this.props}>
        {this.content}
      </SlimContainer>
    )
  }
}

export default LoginView;
