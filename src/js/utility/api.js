const apiBase = '/api';

function POST(uri, options) {
  const { data, ...rest } = options;
  return fetch(uri, {
    body: JSON.stringify(data),
    headers: {
      'Content-type': 'application/json',
    },
    method: 'POST',
    cache: 'no-cache',
    mode: 'cors',
    ...rest,
  })
    .then(response => response.json());
}

function GET(uri, options) {
  return fetch(uri, {
    credentials: 'same-origin',
    ...options,
  })
    .then(response => response.json());
}

export function googleLogin(authToken) {
  return POST(`${apiBase}/login`, {
    data: { google_token: authToken },
  });
}

export function getTrainings() {
  return GET(`${apiBase}/trainings/`).then(response => response.results);
}

export function logout() {
  return GET(`${apiBase}/logout`);
}
