/* eslint-disable import/first */
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import PrivateRoute from './private-route';

import LoginPage from './views/Pages/Login';
import { FullContainer } from './containers';
import Home from './views/Home';
import Trainings from './views/Trainings';

// Dashboard Routes to Home page
export const DashboardRoutes = (
  <Switch>
    <Route exact path="/" name="Home" component={Home} />
    <Route exact path="/trainings" name="Trainings" component={Trainings} />
  </Switch>
);

// Dashboard Routes to Login page
export const RootRoutes = (
  <Switch>
    <Route exact path="/login" name="Login" component={LoginPage} />
    <PrivateRoute path="/" component={FullContainer} routes={DashboardRoutes} />
  </Switch>
);
