import React, { PureComponent } from 'react';
import { UnauthenticatedHeader, Footer } from '../../CoreUI';

class SlimContainer extends PureComponent {
  render() {
    const { children } = this.props;
    return (
      <div className="app">
        <UnauthenticatedHeader brandUrl={__CONFIG__.brandUrl} />
        <div className="app-body">
          <main className="main">
            {children}
          </main>
        </div>
        <Footer />
      </div>
    );
  }
}

export default SlimContainer;
