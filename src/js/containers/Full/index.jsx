import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Container } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import { push } from 'react-router-redux';
import { Header, Sidebar, Footer } from '../../CoreUI';
import { TrainingSidebarSearchForm } from '../../common/components/TrainingSidebarSearchForm';
import { getUserData } from '../../redux/selectors/sessionSelector';

const HeaderWithRouter = withRouter(props => <Header {...props} />);

const mapStateToProps = state => ({
  userData: getUserData(state),
});

const mapDispatchToProps = dispatch => ({
  onMenuItemClick: (item, e) => {
    let action = {};
    if (item.action) {
      action = { type: item.action, payload: e };
    } else if (item.url) {
      action = push(item.url);
    }
    dispatch(action);
  },
});

function FullContainer({ routes, userData, onMenuItemClick }) {
  const appConfig = __CONFIG__;
  return (
    <div className="app">
      <HeaderWithRouter
        brandUrl={appConfig.brandUrl}
        userData={userData}
        userMenu={appConfig.userMenu}
        onMenuItemClick={onMenuItemClick}
      />
      <div className="app-body">
        <Sidebar nav={appConfig.nav} formComponent={TrainingSidebarSearchForm} />
        <main className="main">
          <Container fluid>
            {routes}
          </Container>
        </main>
      </div>
      <Footer />
    </div>
  );
}

FullContainer.propTypes = {
  routes: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(FullContainer);
