import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { isAuthenticated } from './redux/selectors/sessionSelector';

const mapStateToProps = state => ({
  isAuthenticated: isAuthenticated(state),
});

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      const componentParams = { ...props, ...rest };
      return rest.isAuthenticated ? (
        <Component privateRoute {...componentParams} />
      ) : (
        <div>
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location },
            }}
          />
        </div>
      )
    }}
  />
);

export default connect(mapStateToProps)(PrivateRoute);
