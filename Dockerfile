FROM node:6.11.1

ADD ./docker/start.sh /start.sh

RUN chmod +x /start.sh \
    && mkdir -p /app \
    && npm install -g copy-webpack-plugin

VOLUME ["/app"]

WORKDIR /app

EXPOSE 3001 3002

CMD ["/start.sh"]
